package app.pento;

public class Piece {
    private int[] pieceData;
    private int pieceNumber;
    private boolean hasLeftTopSquare;

    public Piece(int pieceNumber, int[] pieceData, boolean hasLeftTopSquare) {
        this.pieceNumber = pieceNumber;
        this.pieceData = pieceData;
        this.hasLeftTopSquare = hasLeftTopSquare;
    }

    public int[] getPieceData() {
        return pieceData;
    }

    public int getSquaresAmount() {
        return pieceData.length/2 + 1;
    }

    public boolean isHasLeftTopSquare() {
        return hasLeftTopSquare;
    }

    public void setHasLeftTopSquare(boolean hasLeftTopSquare) {
        this.hasLeftTopSquare = hasLeftTopSquare;
    }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public void setPieceNumber(int pieceNumber) {
        this.pieceNumber = pieceNumber;
    }
}

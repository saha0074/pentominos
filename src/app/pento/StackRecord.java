package app.pento;

public class StackRecord {
    private int pieceId;
    private int row;
    private int col;

    public StackRecord(int pieceId, int row, int col) {
        this.pieceId = pieceId;
        this.row = row;
        this.col = col;
    }

    public int getPieceId() {
        return pieceId;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isTetramino() {
        return getPieceId() < 4;
    }
}

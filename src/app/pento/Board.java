package app.pento;

public class Board {
    private int rows = 3;
    private int cols = 3;
    private int [][] data;

    public Board(int maxRows, int maxCols) {
        rows = maxRows;
        cols = maxCols;
        data = new int[rows][cols];
    }

    public int getValue(int row, int col) {
        if(hasBadCoordinates(row, col)) {
            return -1;
        }
        return data[row][col];
    }

    public boolean markSquare(int row, int col) {
        return setValue(row, col, 1);
    }

    public boolean unMarkSquare(int row, int col) {
        return setValue(row, col, 0);
    }

    private boolean setValue(int row, int col, int i) {
        if (hasBadCoordinates(row, col)) {
            return false;
        }
        data[row][col] = i;
        return true;
    }

    public boolean isSquareFilled(int row, int col) {
        return getValue(row, col) == 1;
    }

    private boolean hasBadCoordinates(int row, int col) {
        return row >= rows || col >= cols;
    }

    static class Color {
        private static int index = 0;

        private static java.awt.Color colors[] = {
                new java.awt.Color(124, 255, 0),
                new java.awt.Color(194, 255, 0),
                new java.awt.Color(255, 233, 0),
                new java.awt.Color(255, 184, 0),
                new java.awt.Color(255, 108, 0),
                new java.awt.Color(255, 53, 0),
                new java.awt.Color(255, 0, 61),
                new java.awt.Color(255, 0, 141),
                new java.awt.Color(218, 0, 255),
                new java.awt.Color(100, 0, 222),
                new java.awt.Color(23, 0, 222),
                new java.awt.Color(0, 52, 244),
                new java.awt.Color(0, 150, 255),
                new java.awt.Color(0, 225, 255),
                new java.awt.Color(0, 255, 255),
                new java.awt.Color(0, 255, 195),
                new java.awt.Color(0, 255, 89),
                new java.awt.Color(64, 200, 149),
                new java.awt.Color(191, 255, 137),
                new java.awt.Color(200, 159, 97),
                new java.awt.Color(217, 161, 255),
                new java.awt.Color(209, 241, 255)
        };

        public static java.awt.Color getNextColor() {
            return colors[index++ % colors.length];
        }
    }
}

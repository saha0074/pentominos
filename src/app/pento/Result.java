package app.pento;

import java.util.ArrayList;
import java.util.List;

public class Result {
    private int triminoAmount = 0;
    private int tetraminoAmount = 0;
    private List<List<StackRecord>> varaints = new ArrayList<List<StackRecord>>();

    public Result(int triminoAmount, int tetraminoAmount) {
        this.triminoAmount = triminoAmount;
        this.tetraminoAmount = tetraminoAmount;
    }

    public void addVariant(List<StackRecord> variant) {
        varaints.add(variant);
    }

    public int getTriminoAmount() {
        return triminoAmount;
    }

    public void setTriminoAmount(int triminoAmount) {
        this.triminoAmount = triminoAmount;
    }

    public int getTetraminoAmount() {
        return tetraminoAmount;
    }

    public void setTetraminoAmount(int tetraminoAmount) {
        this.tetraminoAmount = tetraminoAmount;
    }
}

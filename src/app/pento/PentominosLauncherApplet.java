package app.pento;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PentominosLauncherApplet extends JApplet {

    private JButton launchButton;
    private Pentominos frame;

    public PentominosLauncherApplet() {
        launchButton = new JButton("Launch Pentominos");
        setContentPane(launchButton);
        launchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                doLaunchButton();
            }
        });
        doLaunchButton();
    }

    public void stop() {
        if (frame != null) {
            frame.dispose();
        }
    }

    private void doLaunchButton() {
        launchButton.setEnabled(false);
        if (frame == null) {
            frame = new Pentominos("Pentominos", 7, 7);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent evt) {
                    launchButton.setText("Запустити розв’язання");
                    launchButton.setEnabled(true);
                    frame = null;
                }

                public void windowOpened(WindowEvent evt) {
                    launchButton.setText("Зупинити розв’язання");
                    launchButton.setEnabled(true);
                }
            });
            frame.setVisible(true);
        } else {
            frame.dispose();
        }
    }
}
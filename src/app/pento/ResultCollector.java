package app.pento;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ResultCollector {
    private Map<Integer, Result> results = new ConcurrentHashMap<Integer, Result>();

    public synchronized void addResult(List<StackRecord> stackRecords) {
        int tri = 0;
        int tetra = 0;
        for (StackRecord stackRecord : stackRecords) {
            if(stackRecord.isTetramino()) {
                tetra++;
            } else {
                tri++;
            }
        }
        Result result = results.get(tri);
        if(result != null) {
            result.addVariant(stackRecords);
        } else {
            result = new Result(tri, tetra);
            result.addVariant(stackRecords);
        }
        results.put(tri, result);
    }

}

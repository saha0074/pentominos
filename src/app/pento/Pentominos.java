package app.pento;

import javax.swing.*;
import java.awt.*;

public class Pentominos extends JFrame {

    public static void main(String[] args) {
        int rows, cols;
        try {
            rows = Integer.parseInt(args[0]);
            cols = Integer.parseInt(args[1]);
        } catch (Exception e) {
            rows = 7;
            cols = 7;
        }
        Pentominos pentominos = new Pentominos("Pentominos", rows, cols);
        pentominos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pentominos.setVisible(true);
    }

    public Pentominos(String title, int rows, int cols) {
        super(title);
        PentominosPanel panel = new PentominosPanel(rows, cols);
        setContentPane(panel);
        setJMenuBar(panel.getMenuBar());
        pack();
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screensize.width - getWidth()) / 2, (screensize.height - getHeight()) / 2);
    }

}
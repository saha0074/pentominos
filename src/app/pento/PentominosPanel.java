package app.pento;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Stack;

public class PentominosPanel extends JPanel {

    private MosaicPanel graphicBoard;
    private JLabel comment;
    private JMenuItem goAction, quitAction;
    private GameThread gameThread = null;

    private ResultCollector resultCollector = new ResultCollector();

    private static final int NO_MESSAGES = 0;

    private static final int GO_MESSAGE = 1;
    private static final int NO_SOLUTIONS = 2;
    private static int savedImagesCounter = 0;

    private int rows, cols;

    private ActionListener menuHandler = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            Object source = evt.getSource();
            if (source == goAction) {
                gameThread.setMessage(GO_MESSAGE);
            } else if (source == quitAction) {
                System.exit(0);
            }
        }
    };

    private static final Piece[] pieces = {
            new Piece(1, new int[]{1, 0, 1, 1, 2, 1}, true),
            new Piece(1, new int[]{1, 0, 1, -1, 2, -1}, true),
            new Piece(1, new int[]{0, 1, 1, 1, 1, 2}, true),
            new Piece(1, new int[]{0, 1, 1, -1, 1, 0}, true),
            new Piece(2, new int[]{0, 1, 1, 1}, true),
            new Piece(2, new int[]{0, 1, 1, 0}, true),
            new Piece(2, new int[]{1, 0, 1, 1}, true),
            new Piece(2, new int[]{1, 0, 1, -1}, true)
    };

    private File folderToSave;

    private int confirmCancelingResult = -1;
    private Board board;

    public PentominosPanel(int rowCt, int colCt) {
        board = new Board(rowCt, colCt);

        setLayout(new BorderLayout(5, 5));
        setBackground(Color.LIGHT_GRAY);
        setRowCol(rowCt, colCt);
        graphicBoard = getGraphicBoard(rowCt, colCt);
        add(graphicBoard, BorderLayout.CENTER);

        comment = new JLabel("", JLabel.CENTER);
        comment.setFont(new Font("TimesRoman", Font.BOLD, 14));
        add(comment, BorderLayout.SOUTH);

        JPanel right = new JPanel();
        right.setLayout(new GridLayout(6, 1, 5, 5));
        goAction = new JMenuItem("Go");
        quitAction = new JMenuItem("Quit");

        String commandKey = "control ";
        try {
            String OS = System.getProperty("os.name");
            if (OS.startsWith("Mac"))
                commandKey = "meta ";
        } catch (Exception ignored) {
        }

        goAction.addActionListener(menuHandler);
        quitAction.addActionListener(menuHandler);
        goAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "G"));
        quitAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "Q"));

        setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 5));
        graphicBoard.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 2));

        gameThread = new GameThread();

        gameThread.start();
    }

    private void setRowCol(int rowCt, int colCt) {
        rows = rowCt < 2 ? 2 : rowCt;
        cols = colCt < 2 ? 2 : colCt;
    }

    private MosaicPanel getGraphicBoard(int rowCt, int colCt) {
        int size = getScreenMaxSize();
        MosaicPanel graphicBoard = new MosaicPanel(rowCt, colCt, size, size);
        graphicBoard.setAlwaysDrawGrouting(true);
        graphicBoard.setDefaultColor(Color.WHITE);
        graphicBoard.setGroutingColor(Color.LIGHT_GRAY);
        return graphicBoard;
    }

    private int getScreenMaxSize() {
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        int rowsize = (screensize.height - 100) / rows;
        if (rowsize > 35) {
            rowsize = 35;  // Maximum size for squares
        } else if (rowsize < 4) {
            rowsize = 4;
        }
        int colsize = (screensize.width - 50) / rows;
        if (colsize > 35) {
            colsize = 35;  // Maximum size for squares
        } else if (colsize < 4) {
            colsize = 4;
        }
        return Math.min(rowsize, colsize);
    }

    public JMenuBar getMenuBar() {
        JMenuBar bar = new JMenuBar();
        JMenu control = new JMenu("Control");
        control.add(goAction);
        control.addSeparator();
        control.add(quitAction);
        bar.add(control);
        return bar;
    }

    private void doSaveImage() {
        if (confirmCancelingResult != JOptionPane.OK_OPTION) {
            BufferedImage image = graphicBoard.getImage();

            String fileName = "pentominos_" + rows + "x" + cols + "_" + savedImagesCounter++ + ".png";
            File selectedFile = new File(fileName);
            if (this.folderToSave == null) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setSelectedFile(selectedFile);
                fileDialog.setDialogTitle("Зберегти зображення як PNG...");
                int option = fileDialog.showSaveDialog(graphicBoard);
                if (option != JFileChooser.APPROVE_OPTION) {
                    this.confirmCancelingResult = JOptionPane.showConfirmDialog(graphicBoard, "Не зберігати картинки розв’язків?");
                    return;
                }
                this.folderToSave = fileDialog.getSelectedFile();
            }
            try {
                ImageIO.write(image, "PNG", new File(folderToSave.getAbsolutePath() + fileName));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(graphicBoard, "Помилка при збереженні файлу:\n" + e.getMessage());
            }
        }
    }

    private class GameThread extends Thread {

        volatile int message = 0;

        private Stack<StackRecord> stack = new Stack<StackRecord>();

        int row = 0;
        int col = 0;
        private int pieceId;

        public void run() {
            while (true) {
                try {
                    graphicBoard.repaint();
                    graphicBoard.setAutopaint(true);

                    doDelay(25);

                    comment.setText("Solving...");
                    setMessage(NO_MESSAGES);

                    play();

                    if (message == NO_SOLUTIONS) {
                        comment.setText("Done");
                        doDelay(-1);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(PentominosPanel.this, "An internal error has occurred:\n" + e);
                    e.printStackTrace();
                    graphicBoard.setAutopaint(true);
                }
            }
        }

        void play() {
            while (true) {
                if (pieceId >= 0 && tryPutPiece(pieceId)) {
                    stack.push(new StackRecord(pieceId, row, col));
                    getNextRowCol();
                    pieceId = 0;
                } else {
                    if (pieceId < 0) {
                        if (stack.isEmpty()) {
                            JOptionPane.showMessageDialog(graphicBoard, "Більше розв’язків немає");
                            setMessage(NO_SOLUTIONS);
                            return;
                        }
                        popStack();
                    }
                    setNextPieceId();
                }
                if (row == rows - 1) {
                    popStack();
                    setNextPieceId();
                }
                if (isSolved()) {
                    resultCollector.addResult(stack);
                    drawStackOnGraphicBoard();
//                    doSaveImage();
                    popStack();
                    setNextPieceId();
                }
            }
        }

        boolean tryPutPiece(int pieceId) {
            if (board.isSquareFilled(row, col))
                return false;
            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
                if (badPiece(pieceId, i)) {
                    return false;
                }
            }
            fillPiece(pieceId, row, col);
            return true;
        }

        private void getNextRowCol() {
            for (int i = row; i < rows; i++) {
                for (int j = col; j < cols; j++) {
                    if (!board.isSquareFilled(i, j)) {
                        row = i;
                        col = j;
                        return;
                    }
                }
                col = 0;
            }
            row = -1;
            col = -1;
        }

        private void popStack() {
            StackRecord stackRecord = stack.pop();
            row = stackRecord.getRow();
            col = stackRecord.getCol();
            pieceId = stackRecord.getPieceId();
            unFillPiece(pieceId, row, col);
        }

        private void setNextPieceId() {
            pieceId = pieceId + 1 < pieces.length ? pieceId + 1 : -1;
        }

        private boolean badPiece(int pieceId, int i) {
            return hasNotTopRightSquare(pieceId, row, col, i)
                    || hasNotNeededSpace(pieceId, row, col, i)
                    || pieceIsBiggerThanNeeded(pieceId, row, col, i);
        }

        private boolean hasNotNeededSpace(int pieceId, int row, int col, int i) {
            return board.isSquareFilled(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1]);
        }

        private boolean hasNotTopRightSquare(int pieceId, int row, int col, int i) {
            return row + pieces[pieceId].getPieceData()[i] < 0
                    || col + pieces[pieceId].getPieceData()[i + 1] < 0;
        }

        private boolean pieceIsBiggerThanNeeded(int pieceId, int row, int col, int i) {
            return row + pieces[pieceId].getPieceData()[i] >= rows
                    || col + pieces[pieceId].getPieceData()[i + 1] >= cols;
        }

        private void drawStackOnGraphicBoard() {
            for (StackRecord stackRecord : stack) {
                drawPiece(stackRecord.getPieceId(), stackRecord.getRow(), stackRecord.getCol());
            }
            graphicBoard.repaint();
        }

        private void drawPiece(int pieceId, int row, int col) {
            Color color = Board.Color.getNextColor();
            graphicBoard.setColor(row, col, color);
            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
                graphicBoard.setColor(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1], color);
            }
        }

        private void fillPiece(int pieceId, int row, int col) {
            board.markSquare(row, col);
            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
                board.markSquare(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1]);
            }
        }

        private void unFillPiece(int pieceId, int row, int col) {
            board.unMarkSquare(row, col);
            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
                board.unMarkSquare(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1]);
            }
        }

        private boolean isSolved() {
            return row < 0 || col < 0;
        }

        synchronized void doDelay(int milliseconds) {
            if (milliseconds < 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            } else {
                try {
                    wait(milliseconds);
                } catch (InterruptedException e) {
                }
            }
        }

        synchronized void setMessage(int message) {
            this.message = message;
            if (message > 0) {
                notify();
            }
        }
    }
}
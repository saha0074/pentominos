//package app.arch;
//
//import app.pento.MosaicPanel;
//import app.pento.Piece;
//import app.pento.StackRecord;
//
//import javax.imageio.ImageIO;
//import javax.swing.*;
//import java.awt.*;
//import java.awt.event.*;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.net.URL;
//import java.util.Random;
//import java.util.Stack;
//
//public class PentominosPanel extends JPanel {
//
//    private MosaicPanel board;  // for displaying the board on the screen
//
//    private JLabel comment;   // status comment displayed under the board
//
//    private Random random = new Random();
//
//    private Stack<StackRecord> stack = new Stack<StackRecord>();
//
//    private GameThread gameThread = null;   // a thread to run the puzzle solving procedure
//
//    private JMenuItem restartAction, restartClearAction, restartRandomAction;  // Menu items for user commands
//    private JMenuItem goAction, pauseAction, stepAction, saveAction, quitAction;
//    private JMenuItem oneSidedAction;
//    private JCheckBoxMenuItem randomizePiecesChoice, checkForBlocksChoice, symmetryCheckChoice;
//
//    private JRadioButtonMenuItem[] speedChoice = new JRadioButtonMenuItem[7];  // Menu items for setting the speed
//
//    private final int[] speedDelay = {5, 25, 100, 500, 1000};  // delay times between moves for speeds 2--6
//
//    volatile private int selectedSpeed = 4;  // initial default speed and corresponding delay
//    volatile private int delay = 100;
//
//    private boolean creatingBoard;  // this is true when user is setting up a board
//    private int clickCt;  // number of squares that have been blackened by the user -- see the mousePressed routine
//
//    private final static int GO_MESSAGE = 1;      // the values for the message variable
//    private final static int STEP_MESSAGE = 2;
//    private final static int PAUSE_MESSAGE = 3;
//    private final static int RESTART_MESSAGE = 4;
//    private final static int RESTART_CLEAR_MESSAGE = 5;
//    private final static int RESTART_RANDOM_MESSAGE = 6;
//    private final static int TERMINATE_MESSAGE = 7;
//
//
//    private int rows, cols;  // Number of rows and columns in the board.
//
//    private int piecesNeeded; // How many pieces are needed to fill board as much as possible.  Always <= 12
//    private int spareSpaces;  // Number of extra spaces after piecesNeeded pieces have been placed.
//
//
//    private MouseListener mouseHandler = new MouseAdapter() {
//        /**
//         * The MousePressed routine handles slection of spaces that are to be left empty.
//         * When all empty spaces have been selected, the process of finding the solution is begun.
//         */
//        public void mousePressed(MouseEvent evt) {
//            if (creatingBoard) {
//                int col = board.xCoordToColumnNumber(evt.getX());
//                int row = board.yCoordToRowNumber(evt.getY());
//                if (col < 0 || col >= cols || row < 0 || row >= rows)
//                    return;
//                if (board.getColor(row, col) == null && clickCt < spareSpaces) {
//                    board.setColor(row, col, emptyColor);
//                    clickCt++;
//                    if (clickCt == spareSpaces)
//                        comment.setText("Use \"Go\" to Start (or click a black square)");
//                    else
//                        comment.setText("Click (up to) " + (spareSpaces - clickCt) + " squares.");
//                } else if (board.getColor(row, col) != null && clickCt > 0) {
//                    board.setColor(row, col, null);
//                    clickCt--;
//                    comment.setText("Click (up to) " + (spareSpaces - clickCt) + " squares.");
//                }
//            }
//        }
//
//    };
//
//    private ActionListener menuHandler = new ActionListener() {
//        public void actionPerformed(ActionEvent evt) {
//            Object source = evt.getSource();
//            if (source == restartAction) {
//                pauseAction.setEnabled(false);
//                stepAction.setEnabled(false);
//                gameThread.setMessage(RESTART_MESSAGE);
//            } else if (source == restartClearAction) {
//                pauseAction.setEnabled(false);
//                stepAction.setEnabled(false);
//                gameThread.setMessage(RESTART_CLEAR_MESSAGE);
//            } else if (source == restartRandomAction) {
//                pauseAction.setEnabled(false);
//                stepAction.setEnabled(false);
//                gameThread.setMessage(RESTART_RANDOM_MESSAGE);
//            } else if (source == goAction) {
//                pauseAction.setEnabled(true);
//                stepAction.setEnabled(false);
//                gameThread.setMessage(GO_MESSAGE);
//            } else if (source == pauseAction) {
//                pauseAction.setEnabled(false);
//                stepAction.setEnabled(true);
//                gameThread.setMessage(PAUSE_MESSAGE);
//            } else if (source == stepAction) {
//                gameThread.setMessage(STEP_MESSAGE);
//            } else if (source == checkForBlocksChoice)
//                gameThread.checkForBlocks = checkForBlocksChoice.isSelected();
//            else if (source == randomizePiecesChoice)
//                gameThread.randomizePieces = randomizePiecesChoice.isSelected();
//            else if (source == symmetryCheckChoice)
//                gameThread.symmetryCheck = symmetryCheckChoice.isSelected();
//            else if (source == oneSidedAction)
//                doOneSidedCommand();
//            else if (source == saveAction)
//                doSaveImage();
//            else if (source == quitAction)
//                System.exit(0);
//            else if (source instanceof JRadioButtonMenuItem) {
//                JRadioButtonMenuItem item = ((JRadioButtonMenuItem) source);
//                int i;
//                for (i = 0; i < speedChoice.length; i++) {
//                    if (speedChoice[i] == item)
//                        break;
//                }
//                if (i == speedChoice.length || i == selectedSpeed)
//                    return;
//                selectedSpeed = i;
//                if (selectedSpeed < 2)
//                    delay = 0;
//                else
//                    delay = speedDelay[selectedSpeed - 2];
//                if (gameThread.running)
//                    board.setAutopaint(selectedSpeed > 1);
//                board.repaint();
//                gameThread.doDelay(25);
//            }
//        }
//    };
//
//    /**
//     * This data structure represents the pieces.  There are 12 pieces, and each piece can be rotated
//     * and flipped over.  Some of these motions leave the peice changed because of symmetry.  Each distinct
//     * position of each piece has a line in this array.  Each line has 9 elements.  The first element is
//     * the number of the piece, from 1 to 12.  The remaining 8 elements describe the shape of the piece
//     * in the following peculiar way:  One square is assumed to be at position (0,0) in a grid; the square is
//     * chosen as the "top-left" square in the piece, in the sense that all the other squares are either to the
//     * right of this square in the same row, or are in lower rows.  The remaining 4 squares in the piece are
//     * encoded by 8 numbers that give the row and column of each of the remaining squares.   If the eight numbers
//     * that describe the piece are (a,b,c,d,e,f,g,h) then when the piece is placed on the board with the top-left
//     * square at position (r,c), the remaining squares will be at positions (r+a,c+b), (r+c,c+d), (r+e,c+f), and
//     * (r+g,c+h).  this representation is used in the putPiece() and removePiece() metthods.
//     */
////    private static final int[][] piece_data = {
////            {1, 1, 0, 1, 1, 2, 1},
////            {1, 1, 0, 1, -1, 2, -1},
////            {1, 0, 1, 1, 1, 1, 2},
////            {1, 0, -1, 1, -1, 1, -2},
////            {2, 0, 1, 1, 1},
////            {2, 0, 1, 1, 0},
////            {2, 1, 0, 1, 1},
////            {2, 1, 0, 1, -1}
////    };
//
//    private static final Random random1 = new Random();
//    private static final Piece[] pieces = {
//            new Piece(1, new int[]{1, 0, 1, 1, 2, 1}, true, random1),
//            new Piece(1, new int[]{1, 0, 1, -1, 2, -1}, true, random1),
//            new Piece(1, new int[]{0, 1, 1, 1, 1, 2}, true, random1),
//            new Piece(1, new int[]{0, 1, 1, -1, 1, 0}, true, random1),
//            new Piece(2, new int[]{0, 1, 1, 1}, true, random1),
//            new Piece(2, new int[]{0, 1, 1, 0}, true, random1),
//            new Piece(2, new int[]{1, 0, 1, 1}, true, random1),
//            new Piece(2, new int[]{1, 0, 1, -1}, true, random1)
//    };
//
//    private Color pieceColor[] = {  // the colors of pieces number 1 through 12; pieceColor[0] is not used.
//            null,
//            new Color(200, 0, 0),
//            new Color(150, 150, 255),
//            new Color(0, 200, 200),
//            new Color(255, 150, 255),
//            new Color(0, 200, 0),
//            new Color(150, 255, 255),
//            new Color(200, 200, 0),
//            new Color(0, 0, 200),
//            new Color(255, 150, 150),
//            new Color(200, 0, 200),
//            new Color(255, 255, 150),
//            new Color(150, 255, 150)
//    };
//
//    private final static Color emptyColor = Color.BLACK; // the color of a square that the user has seleted to be left empty.
//
//
//    /**
//     * Create a pentominos board with 8 rows and 8 columns.
//     */
//    public PentominosPanel() {
//        this(8, 8, true);
//    }
//
//    /**
//     * Create a pentominos board with a specified number of rows and columns, which must be 3 or greater.
//     * If autostart is true, the program creates a random board and starts solving immediately.
//     */
//    public PentominosPanel(int rowCt, int colCt, boolean autostart) {
//
//        setLayout(new BorderLayout(5, 5));
//        setBackground(Color.LIGHT_GRAY);
//
//        rows = rowCt;
//        if (rows < 3)
//            rows = 8;
//        if (cols < 3)
//            cols = 8;
//        cols = colCt;
//
//        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
//        int rowsize = (screensize.height - 100) / rows;
//        if (rowsize > 35)
//            rowsize = 35;  // Maximum size for squares
//        else if (rowsize < 4)
//            rowsize = 4;
//        int colsize = (screensize.width - 50) / rows;
//        if (colsize > 35)
//            colsize = 35;  // Maximum size for squares
//        else if (colsize < 4)
//            colsize = 4;
//        int size = Math.min(rowsize, colsize);
//        board = new MosaicPanel(rowCt, colCt, size, size);  // for displaying the board
//        board.setAlwaysDrawGrouting(true);
//        board.setDefaultColor(Color.WHITE);
//        board.setGroutingColor(Color.LIGHT_GRAY);
//        add(board, BorderLayout.CENTER);
//
//        comment = new JLabel("", JLabel.CENTER);
//        comment.setFont(new Font("TimesRoman", Font.BOLD, 14));
//        add(comment, BorderLayout.SOUTH);
//
//        JPanel right = new JPanel();                // holds control buttons
//        right.setLayout(new GridLayout(6, 1, 5, 5));
//        restartAction = new JMenuItem("Restart");
//        restartClearAction = new JMenuItem("Restart / Empty Board");
//        restartRandomAction = new JMenuItem("Restart / Random");
//        goAction = new JMenuItem("Go");
//        pauseAction = new JMenuItem("Pause");
//        stepAction = new JMenuItem("Step");
//        saveAction = new JMenuItem("Save Image...");
//        quitAction = new JMenuItem("Quit");
//        randomizePiecesChoice = new JCheckBoxMenuItem("Randomize Order of Pieces");
//        checkForBlocksChoice = new JCheckBoxMenuItem("Check for Obvious Blocking");
//        symmetryCheckChoice = new JCheckBoxMenuItem("Symmetry Check");
//        oneSidedAction = new JMenuItem("One Sided [Currently OFF]...");
//
//        String commandKey;
//        commandKey = "control ";
//        try {
//            String OS = System.getProperty("os.name");
//            if (OS.startsWith("Mac"))
//                commandKey = "meta ";
//        } catch (Exception e) {
//        }
//
//        restartAction.addActionListener(menuHandler);
//        restartClearAction.addActionListener(menuHandler);
//        restartRandomAction.addActionListener(menuHandler);
//        goAction.addActionListener(menuHandler);
//        pauseAction.addActionListener(menuHandler);
//        stepAction.addActionListener(menuHandler);
//        saveAction.addActionListener(menuHandler);
//        quitAction.addActionListener(menuHandler);
//        randomizePiecesChoice.addActionListener(menuHandler);
//        checkForBlocksChoice.addActionListener(menuHandler);
//        symmetryCheckChoice.addActionListener(menuHandler);
//        oneSidedAction.addActionListener(menuHandler);
//        goAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "G"));
//        pauseAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "P"));
//        stepAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "S"));
//        restartAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "R"));
//        restartClearAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "E"));
//        restartRandomAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "D"));
//        quitAction.setAccelerator(KeyStroke.getKeyStroke(commandKey + "Q"));
//
//        ButtonGroup group = new ButtonGroup();
//        speedChoice[0] = new JRadioButtonMenuItem("Solutions Only / No Stop");
//        speedChoice[1] = new JRadioButtonMenuItem("Very Fast (Limited Graphics)");
//        speedChoice[2] = new JRadioButtonMenuItem("Faster");
//        speedChoice[3] = new JRadioButtonMenuItem("Fast");
//        speedChoice[4] = new JRadioButtonMenuItem("Moderate");
//        speedChoice[5] = new JRadioButtonMenuItem("Slow");
//        speedChoice[6] = new JRadioButtonMenuItem("Slower");
//        for (int i = 0; i < 7; i++) {
//            group.add(speedChoice[i]);
//            speedChoice[i].addActionListener(menuHandler);
//            speedChoice[i].setAccelerator(KeyStroke.getKeyStroke(commandKey + (char) ('0' + i)));
//        }
//        speedChoice[4].setSelected(true);
//
//        board.addMouseListener(mouseHandler);
//
//        piecesNeeded = (rows * cols) / 5;
//        if (piecesNeeded > 12)
//            piecesNeeded = 12;
//        spareSpaces = rows * cols - 5 * piecesNeeded;
//        if (spareSpaces > 0)
//            comment.setText("Click (up to) " + spareSpaces + " squares");
//        creatingBoard = spareSpaces > 0;
//        clickCt = 0;
//
//        setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 5));
//        board.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 2));
//
//        gameThread = new GameThread();
//
//        if (autostart) {
//            gameThread.setMessage(RESTART_RANDOM_MESSAGE);
//            pauseAction.setEnabled(true);
//            stepAction.setEnabled(false);
//            creatingBoard = false;
//        } else {
//            pauseAction.setEnabled(false);
//            creatingBoard = spareSpaces > 0;
//            if (creatingBoard)
//                comment.setText("Select Squares or Use \"Go\" to Start");
//            else
//                comment.setText("Use \"Go\" to Start");
//        }
//
//        gameThread.start();
//
//    }
//
//    /**
//     * Retrun a menu bar containing a Control menu and a Speed menu with the available commands
//     * for this Pentominoes bo
//     *
//     * @param includeSaveAndQuit // If true, Save Image and Quit commands are included in the Control
//     *                           menu [ not suitable for use in an applet.
//     */
//    public JMenuBar getMenuBar(boolean includeSaveAndQuit, PentominosPanel getOptionsFromThisOne) {
//        JMenuBar bar = new JMenuBar();
//        JMenu control = new JMenu("Control");
//        control.add(goAction);
//        control.add(pauseAction);
//        control.add(stepAction);
//        control.addSeparator();
//        control.add(restartAction);
//        if (spareSpaces > 0) {
//            control.add(restartClearAction);
//            control.add(restartRandomAction);
//        }
//        control.addSeparator();
//        control.add(checkForBlocksChoice);
//        control.add(randomizePiecesChoice);
//        if (rows * cols >= 60)
//            control.add(symmetryCheckChoice);  // Only add if the board can hold all 12 pieces
//        control.add(oneSidedAction);
//        if (includeSaveAndQuit) {
//            control.addSeparator();
//            control.add(saveAction);
//            control.addSeparator();
//            control.add(quitAction);
//        }
//        bar.add(control);
//        JMenu speed = new JMenu("Speed");
//        speed.add(speedChoice[0]);
//        speed.addSeparator();
//        for (int i = 1; i < speedChoice.length; i++)
//            speed.add(speedChoice[i]);
//        bar.add(speed);
//        if (getOptionsFromThisOne != null) {
//            gameThread.randomizePieces = getOptionsFromThisOne.randomizePiecesChoice.isSelected();
//            randomizePiecesChoice.setSelected(gameThread.randomizePieces);
//            gameThread.checkForBlocks = (getOptionsFromThisOne.checkForBlocksChoice.isSelected());
//            checkForBlocksChoice.setSelected(gameThread.checkForBlocks);
//            if (rows * cols >= 60) {
//                gameThread.symmetryCheck = getOptionsFromThisOne.symmetryCheckChoice.isSelected();
//                symmetryCheckChoice.setSelected(gameThread.symmetryCheck);
//            }
//            gameThread.useOneSidedPieces = getOptionsFromThisOne.gameThread.useOneSidedPieces;
//            if (gameThread.useOneSidedPieces)
//                oneSidedAction.setText("One Sided [Currently ON]...");
//            gameThread.useSideA = getOptionsFromThisOne.gameThread.useSideA;
//            for (int i = 0; i < speedChoice.length; i++)
//                if (getOptionsFromThisOne.speedChoice[i].isSelected()) {
//                    speedChoice[i].setSelected(true);
//                    selectedSpeed = i;
//                    if (selectedSpeed < 2)
//                        delay = 0;
//                    else
//                        delay = speedDelay[selectedSpeed - 2];
//                    break;
//                }
//        }
//        return bar;
//    }
//
//    /**
//     * Save a PNG image of the current board in a user-selected file.
//     */
//    private void doSaveImage() {
//        BufferedImage image = board.getImage();  // The image currently displayed in the MosaicPanel.
//        JFileChooser fileDialog = new JFileChooser();
//        String defaultName = "pentominos_" + rows + "x" + cols + ".png"; // Default name for file to be saved.
//        File selectedFile = new File(defaultName);
//        fileDialog.setSelectedFile(selectedFile);
//        fileDialog.setDialogTitle("Save Image as PNG File");
//        int option = fileDialog.showSaveDialog(board);  // Presents the "Save File" dialog to the user.
//        if (option != JFileChooser.APPROVE_OPTION)
//            return;  // user canceled
//        selectedFile = fileDialog.getSelectedFile();  // The file the user has elected to save.
//        if (selectedFile.exists()) {
//            int response = JOptionPane.showConfirmDialog(board,
//                    "The file \"" + selectedFile.getName() + "\" already exists.\nDo you want to replace it?",
//                    "Replace file?",
//                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
//            if (response == JOptionPane.NO_OPTION)
//                return; // user does not want to replace existing file
//        }
//        try {
//            if (!ImageIO.write(image, "PNG", selectedFile))  // This actually writes the image to the file.
//                JOptionPane.showMessageDialog(board, "Sorry, it looks like PNG files are not supported!");
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(board, "Sorry, an error occurred while trying to save the file:\n" + e.getMessage());
//        }
//    }
//
//    private void doOneSidedCommand() { // Called when user selects the "One Sided" command.
//        final JRadioButton[][] radioButtons = new JRadioButton[6][2];
//        JPanel[][] buttonPanels = new JPanel[6][2];
//        boolean[] newUseSideA = gameThread.useSideA == null ? new boolean[]{true, true, true, true, true, true} : (boolean[]) gameThread.useSideA.clone();
//        boolean newUseOneSidedPieces = gameThread.useOneSidedPieces;
//        JCheckBox enableCheckBox;
//        try {
//            Icon icon;
//            ClassLoader classLoader = getClass().getClassLoader();
//            Toolkit toolkit = Toolkit.getDefaultToolkit();
//            for (int i = 0; i < 6; i++) {
//                ButtonGroup group = new ButtonGroup();
//                for (int j = 0; j < 2; j++) {
//                    URL imageURL = classLoader.getResource("pics/piece" + i + "_side" + (j + 1) + ".png");
//                    if (imageURL == null)
//                        throw new Exception();
//                    icon = new ImageIcon(toolkit.createImage(imageURL));
//                    radioButtons[i][j] = new JRadioButton("");
//                    if (!newUseOneSidedPieces)
//                        radioButtons[i][j].setEnabled(false);
//                    group.add(radioButtons[i][j]);
//                    buttonPanels[i][j] = new JPanel();
//                    buttonPanels[i][j].setLayout(new BorderLayout(5, 5));
//                    buttonPanels[i][j].add(radioButtons[i][j], j == 0 ? BorderLayout.WEST : BorderLayout.EAST);
//                    JLabel label = new JLabel(icon);
//                    buttonPanels[i][j].add(label, BorderLayout.CENTER);
//                    final int k = i, l = j;
//                    label.addMouseListener(new MouseAdapter() {
//                        public void mousePressed(MouseEvent evt) {
//                            if (radioButtons[k][l].isEnabled())
//                                radioButtons[k][l].setSelected(true);
//                        }
//                    });
//                }
//                radioButtons[i][newUseSideA[i] ? 0 : 1].setSelected(true);
//            }
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Internal Error!  Can't find pentomino images.\nThe \"One Sided\" command will be disabled.");
//            oneSidedAction.setEnabled(false);
//            e.printStackTrace();
//            return;
//        }
//        JPanel panel = new JPanel();
//        JPanel main = new JPanel();
//        JPanel top = new JPanel();
//        panel.setLayout(new BorderLayout(10, 10));
//        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
//        panel.add(main, BorderLayout.CENTER);
//        panel.add(top, BorderLayout.NORTH);
//        main.setLayout(new GridLayout(6, 2, 12, 6));
//        for (int i = 0; i < 6; i++) {
//            main.add(buttonPanels[i][0]);
//            main.add(buttonPanels[i][1]);
//        }
//        enableCheckBox = new JCheckBox("Enable One Sided Pieces");
//        enableCheckBox.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent evt) {
//                boolean on = ((JCheckBox) evt.getSource()).isSelected();
//                for (int i = 0; i < 6; i++) {
//                    radioButtons[i][0].setEnabled(on);
//                    radioButtons[i][1].setEnabled(on);
//                }
//            }
//        });
//        enableCheckBox.setSelected(newUseOneSidedPieces);
//        top.setLayout(new GridLayout(2, 1, 25, 25));
//        top.add(enableCheckBox);
//        top.add(new JLabel("Select the side of each piece that you want to use:"));
//        int answer = JOptionPane.showConfirmDialog(this, panel, "Use One Sided Pieces?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (answer == JOptionPane.CANCEL_OPTION)
//            return;
//        newUseOneSidedPieces = enableCheckBox.isSelected();
//        if (!newUseOneSidedPieces) {
//            oneSidedAction.setText("One Sided [Currently OFF]...");
//        } else {
//            for (int i = 0; i < 6; i++)
//                newUseSideA[i] = radioButtons[i][0].isSelected();
//            gameThread.useSideA = newUseSideA;
//            oneSidedAction.setText("One Sided [Currently ON]...");
//        }
//        gameThread.useOneSidedPieces = newUseOneSidedPieces;
//    }
//
//
//    /**
//     * This should be called to terminate the game playing thread just before this PentominosPanel
//     * is discarded.  This is used in the frame clase, Pentominos.java.
//     */
//    synchronized public void terminate() {
//        gameThread.setMessage(TERMINATE_MESSAGE);
//        notify();
//        gameThread.doDelay(25);
//        board = null;
//    }
//
//
//    private class GameThread extends Thread {  // This represents the thread that solves the puzzle.
//
//        int moveCount;        // How many pieces have been placed so far
//        int movesSinceCheck;  // How many moves since the last time the board was redrawn, while running at speed #1
//        int solutionCount;    // How many solutions have been found so far
//
//        volatile boolean running;   // True when the solution process is running (and not when it is paused)
//
//        boolean aborted;  // used in play() to test whether the solution process has been aborted by a "restart"
//
//        volatile int message = 0;  // "message" is used by user-interface thread to send control messages
//        // to the game-playing thread.  A value of  0 indicates "no message"
//
//        volatile boolean randomizePieces;  // If true, the pieces array is put into random order at start of play.
//        volatile boolean checkForBlocks;   // If true, a check is made for obvious blocking.
//        volatile boolean symmetryCheck;    // If true, the symmetry of the board is checked, and if it has any symmetry,
//        // some pieces are removed from the list to avoid redundant solutions.
//        volatile boolean useOneSidedPieces;// If true, only one side of two-sided pieces is used.
//
//        volatile boolean[] useSideA;  // When useOneSidedPieces, this array tells which side to use for each two-sided piece.
//        // The data for the two sides of each piece is stored in side_info.
//
//        int[][] blockCheck;  // Used for checking for blocking.
//        int blockCheckCt;  // Number of times block check has been run -- used in controling recursive counting instead of just using a boolean array.
//        int emptySpaces; // spareSpaces - (number of black spaces); number of spaces that will be empty in a solution
//
//        int squaresLeftEmpty;  // squares actually left empty in the solution so far
//
//        int row = 0;
//        int col = 0;
//        private int pieceId;
//
//        boolean putPiece(int pieceId) {
//            if (board.getColor(row, col) != null)
//                return false;
//            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
//                if (hasNotTopRightSquare(pieceId, row, col, i) || hasNotNeededSpace(pieceId, row, col, i)) {
//                    return false;
//                }
//            }
//            drawPiece(pieceId, row, col);
//            return true;
//        }
//
//        private boolean hasNotNeededSpace(int pieceId, int row, int col, int i) {
//            return board.getColor(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1]) != null;
//        }
//
//        private boolean hasNotTopRightSquare(int pieceId, int row, int col, int i) {
//            return row + pieces[pieceId].getPieceData()[i] < 0
//                    || row + pieces[pieceId].getPieceData()[i] >= rows
//                    || col + pieces[pieceId].getPieceData()[i + 1] < 0
//                    || col + pieces[pieceId].getPieceData()[i + 1] >= cols;
//        }
//
//        private void drawPiece(int pieceId, int row, int col) {
//            board.setColor(row, col, pieces[pieceId].getColor());
//            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
//                board.setColor(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1], pieces[pieceId].getColor());
//            }
//        }
//
//        void removePiece(int pieceId, int row, int col) { // Remove piece pieceId from the board, at position (row,col)
//            board.setColor(row, col, null);
//            for (int i = 0; i < pieces[pieceId].getPieceData().length; i += 2) {
//                board.setColor(row + pieces[pieceId].getPieceData()[i], col + pieces[pieceId].getPieceData()[i + 1], null);
//            }
//        }
//
//        void play() {
//            while (true) {
//                if (pieceId >= 0 && putPiece(pieceId)) {
//                    stack.push(new StackRecord(pieceId, row, col));
//                    getNextRowCol();
//                    pieceId = 0;
//                } else {
//                    if (pieceId < 0) {
//                        if (stack.isEmpty()) {
//                            System.out.println("No solution");
//                            System.exit(0);
//                        }
//                        popStack();
//                    }
//                    setNextPieceId();
//                }
//                if (row == rows - 1) {
//                    popStack();
//                    setNextPieceId();
//                }
////                System.out.println("piece:" + pieceId + " row:" + row + " col:" + col);
////                System.out.println(makeBoard());
//                if (hasNotNextEmptySquare()) {
//                    printStack();
//                    doSaveImage();
//                    popStack();
//                    setNextPieceId();
//                }
//            }
//        }
//
//        /*
//        x + x na 4 raz = x+x nf 3 = x+x na x+x
//
//        x+x * x+x * x+x * x+x   =  x+(x * x)+(x * x+x   =  ((x+x) * x)+x = 2x^2 + 2x
//                                =                    =
//
//         */
//
//        private void popStack() {
//            StackRecord stackRecord = stack.pop();
//            row = stackRecord.getRow();
//            col = stackRecord.getCol();
//            pieceId = stackRecord.getPieceId();
//            removePiece(pieceId, row, col);
//        }
//
//        private void printStack() {
//            System.out.println("\nSolution for " + rows + "x" + cols);
//            for (StackRecord record : stack) {
//                System.out.println("row:" + record.getRow() + " col:" + record.getCol() + " piece: " + record.getPieceId());
//            }
//        }
//
//        private boolean hasNotNextEmptySquare() {
//            return row < 0 || col < 0;
//        }
//
//        private void setNextPieceId() {
//            pieceId = pieceId + 1 < pieces.length ? pieceId + 1 : -1;
//        }
//
//        private String makeBoard() {
//            String[][] strings = new String[rows][cols];
//            for (int i = 0; i < strings.length; i++) {
//                for (int j = 0; j < strings[i].length; j++) {
//                    strings[i][j] = ".";
//                }
//            }
//            for (int i = 0; i < rows; i++) {
//                for (int j = 0; j < cols; j++) {
//                    strings[i][j] = board.getColor(i, j) != null ? "0" : ".";
//                }
//            }
//            StringBuilder stringBuilder = new StringBuilder();
//            for (int i = 0; i < strings.length; i++) {
//                for (int j = 0; j < strings[i].length; j++) {
//                    stringBuilder.append(strings[i][j]);
//                }
//                stringBuilder.append("\n");
//            }
//            stringBuilder.append("\n").append("\n");
//            return stringBuilder.toString();
//        }
//
//        private void getNextRowCol() {
//            for (int i = row; i < rows; i++) {
//                for (int j = col; j < cols; j++) {
//                    if (board.getColor(i, j) == null) {
//                        row = i;
//                        col = j;
//                        return;
//                    }
//                }
//                col = 0;
//            }
//            row = -1;
//            col = -1;
//        }
//
//        synchronized void doDelay(int milliseconds) {
//            // wait for specified time, or until a control message is sent using setMessage()
//            // is generated.  For an indefinite wait, milliseconds should be < 0
//            if (milliseconds < 0) {
//                try {
//                    wait();
//                } catch (InterruptedException e) {
//                }
//            } else {
//                try {
//                    wait(milliseconds);
//                } catch (InterruptedException e) {
//                }
//            }
//        }
//
//
//        synchronized void setMessage(int message) {  // send control message to game thread
//            this.message = message;
//            if (message > 0)
//                notify();  // wake game thread if it is sleeping or waiting for a message (in the doDelay method)
//        }
//
//
//        /**
//         * The run method for the thread that runs the game.
//         */
//        public void run() {
//            while (true) {
//                try {
//                    running = false;
//                    saveAction.setEnabled(true);
//                    board.repaint();
////                    while (message != GO_MESSAGE && message != TERMINATE_MESSAGE) {  // wait for game setup
////                        if (message == RESTART_CLEAR_MESSAGE || message == RESTART_MESSAGE) {
////                            clickCt = 0;
////                            creatingBoard = spareSpaces > 0;
////                            if (message == RESTART_MESSAGE && spareSpaces > 0) {
////                                for (int r = 0; r < rows; r++)
////                                    for (int c = 0; c < cols; c++)
////                                        if (board.getColor(r, c) != emptyColor)
////                                            board.setColor(r, c, null);
////                                        else
////                                            clickCt++;
////                                if (spareSpaces > 0 && clickCt == spareSpaces)
////                                    comment.setText("Use \"Go\" to Start (or click a black square)");
////                                else
////                                    comment.setText("Select Squares or Use \"Go\" to Start");
////                            } else {
////                                board.clear();
////                                if (creatingBoard)
////                                    comment.setText("Click (up to) " + spareSpaces + " squares");
////                                else
////                                    comment.setText("Use \"Go\" to Start");
////                            }
////                            setMessage(0);
////                            doDelay(-1);  // wait forever (for control message to start game)
////                        }
////                    }
//                    if (message == TERMINATE_MESSAGE)
//                        break;
//                    creatingBoard = false;
//                    running = true;
//                    saveAction.setEnabled(false);
//                    board.setAutopaint(delay > 0);
//                    board.repaint();
//                    doDelay(25);
//                    // begin next game
//                    pauseAction.setEnabled(true);
//                    stepAction.setEnabled(false);
//                    comment.setText("Solving...");
//                    message = 0;
//                    int startRow = 0;  // reprsents the upper left corner of the board
//                    int startCol = 0;
//                    while (board.getColor(startRow, startCol) != null) {
//                        startCol++;  // move past any filled squares, since Play(square) assumes the square is empty
//                        if (startCol == cols) {
//                            startCol = 0;
//                            startRow++;
//                        }
//                    }
//                    moveCount = movesSinceCheck = solutionCount = 0;
//                    board.setAutopaint(selectedSpeed > 1);
//                    randomizePiecesChoice.setEnabled(false);
//                    symmetryCheckChoice.setEnabled(false);
//                    oneSidedAction.setEnabled(false);
//                    blockCheck = new int[rows][cols];
//                    blockCheckCt = 0;
//                    emptySpaces = spareSpaces - clickCt;
//                    squaresLeftEmpty = 0;
//                    aborted = false;
//                    boolean blocked = false;
//
//                    play();   // run the recursive algorithm that will solve the puzzle
//
//                    if (message == TERMINATE_MESSAGE)
//                        break;
//                    randomizePiecesChoice.setEnabled(true);
//                    symmetryCheckChoice.setEnabled(true);
//                    oneSidedAction.setEnabled(true);
//                    running = false;
//                    saveAction.setEnabled(true);
//                    board.setAutopaint(true);
//                    board.repaint();
//                    if (!aborted) {
//                        pauseAction.setEnabled(false);
//                        stepAction.setEnabled(false);
//                        if (solutionCount == 0)
//                            comment.setText("Done. No soutions. " + moveCount + " moves.");
//                        else if (solutionCount == 1)
//                            comment.setText("Done. 1 solution. " + moveCount + " moves.");
//                        else
//                            comment.setText("Done. " + solutionCount + " solutions. " + moveCount + " moves.");
//                        if (spareSpaces > 0)
//                            creatingBoard = true;
//                        doDelay(-1);
//                    }
//                    if (message == TERMINATE_MESSAGE)
//                        break;
//                } catch (Exception e) {
//                    JOptionPane.showMessageDialog(PentominosPanel.this, "An internal error has occurred:\n" + e + "\n\nRESTARTING.");
//                    e.printStackTrace();
//                    board.setAutopaint(true);
//                    pauseAction.setEnabled(true);
//                    stepAction.setEnabled(false);
//                    message = RESTART_MESSAGE;
//                    if (e instanceof IllegalStateException) {
//                        message = PAUSE_MESSAGE;
//                    }
//                }
//            }
//        }
//    }
//}
